import java.security.Principal;

public class Principal1{
    public static void main(String[] args){
        System.out.println("==> Calculadoraint --> resultado = " + engine_1((int)6,(int)1).calculate( 5,  5));
        System.out.println("==> CalculadoraLong --> resultado = " + engine_1((long)6,(long)1).calculate(6, 3));

        
    }

    private static  Calculadoraint engine_1(int a, int b){
        return (x,y) -> a*b;
    }

    private static CalculadoraLong engine_1(long a, long b){
        return (x,y) -> a-b;
    }


}